<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class LoanController extends Controller
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        $user = Auth::user();
        if ($user->user_type === "Government"){
            $loans = Loan::query()->orderByDesc("updated_at")->get();
        }else{
            $loans = Loan::query()->where(['user_id'=>$user->id])->orderByDesc("updated_at")->get();
        }

        return Inertia::render("Loans",compact('loans'));
    }


    /**
     * @param $id
     * @return Response|RedirectResponse
     */
    public function edit($id): Response|RedirectResponse
    {
        $loan = Loan::query()->findOrFail($id);
        if($loan->status === "waiting decision"){
            if(auth()->user()->user_type !== "Government" && $loan->user_id !== auth()->id())
                return redirect()->back()->with("error","You are not authorized to perform this request!");

            return Inertia::render("EditLoan",compact("loan"));
        }
        return redirect()->back()->with("error","You cannot edit ".$loan->status." loans.");
    }

    /**
     * @return Response
     */
    public function apply(): Response
    {
        return Inertia::render("ApplyLoan");
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            "narration" => "required",
            "amount" => "required|numeric|min:50|max:1000000"
        ]);

        $loan = Loan::query()->create([
            "user_id" => auth()->id(),
            "narration" => $request->get("narration"),
            "amount" => $request->get("amount"),
            "approved_amount" => $request->get("amount"),
        ]);

        return redirect()->back()->with("message","Loan application submitted successfully.");
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function approve($id): RedirectResponse
    {
        if(auth()->user()->user_type !== "Government")
            return redirect()->back()->with("error","You are not authorized to perform this request!");

        $rec = Loan::query()->findOrFail($id);
        if($rec->status === "waiting decision"){
            if($rec->approved_amount < 1)
                return redirect()->back()->with("error","Approved amount cannot be zero!");

            $rec->update([
                "status" => "approved",
            ]);

            return redirect()->back()->with("message","Loan application approved successfully.");
        }
        return redirect()->back()->with("error","You cannot approve ".$rec->status." loans.");
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function reject($id): RedirectResponse
    {
        if(auth()->user()->user_type !== "Government")
            return redirect()->back()->with("error","You are not authorized to perform this request!");

        $rec = Loan::query()->findOrFail($id);
        if($rec->status === "waiting decision"){
            $rec->update([
                "status" => "denied"
            ]);
            return redirect()->back()->with("message","Loan application rejected successfully.");
        }
        return redirect()->back()->with("error","You cannot reject ".$rec->status." loans.");
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            "narration" => "required",
            "amount" => "required|numeric|min:50|max:1000000",
            "approved_amount" => "required|numeric|min:50|max:1000000"
        ]);

        $rec = Loan::query()->findOrFail($id);

        $approved_amount = $request->get("approved_amount");

        if (auth()->user()->user_type === "Government" && $approved_amount > $rec->amount)
            return redirect()->back()->with("error","Approved amount cannot be more than applied amount. Approved amount: $$approved_amount, applied amount: $".$rec->amount);

        if (auth()->user()->user_type !== "Government")
            $approved_amount = $request->get("amount");


        if($rec->status === "waiting decision"){
            if(auth()->user()->user_type !== "Government" && $rec->user_id !== auth()->id())
                return redirect()->back()->with("error","You are not authorized to perform this request!");

            $rec->update([
                "narration" => $request->get("narration"),
                "amount" => $request->get("amount"),
                "approved_amount" => $approved_amount,
            ]);
            return redirect()->back()->with("message","Loan application updated successfully.");
        }
        return redirect()->back()->with("error","You cannot update ".$rec->status." loans.");
    }
}
