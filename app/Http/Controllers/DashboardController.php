<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class DashboardController extends Controller
{
    /**
     * @return Response
     */
    public function index(): \Inertia\Response
    {
        $user = auth()->user();
        if ($user->user_type === "Government"){
            $loans = Loan::query()->get();
        }else{
            $loans = Loan::query()->where(['user_id'=>$user->id])->get();
        }

        $all_loans = count($loans)?$loans->count():0;
        $pending_loans = count($loans)?$loans->where('status','waiting decision')->count():0;
        $approved_loans = count($loans)?$loans->where('status','approved')->count():0;
        $denied_loans = count($loans)?$loans->where('status','denied')->count():0;

        $users = User::all()->count();

        return Inertia::render('Dashboard',compact('all_loans','pending_loans','approved_loans','denied_loans','users'));
    }

}
