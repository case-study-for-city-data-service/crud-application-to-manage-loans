<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{
    /**
     * @return Response
     */
    public function index(): \Inertia\Response
    {
        $users = User::all();
        return Inertia::render("Users",compact('users'));
    }
}
