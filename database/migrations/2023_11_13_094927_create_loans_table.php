<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->string("narration");
            $table->foreignIdFor(User::class)->index();
            $table->double("amount")->default(0.0);
            $table->double("approved_amount")->default(0.0);
            $table->enum("status",["approved","denied","waiting decision"])->default("waiting decision");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('loans');
    }
};
