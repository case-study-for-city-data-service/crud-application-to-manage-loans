## Loans Manager
Loans Manager APP is a web based application that enables Nonprofit Organization to register and apply for loans from the government. 

As a Nonprofit Organization, to get a loan;

- Register
- Apply for loan
- Get Approval
- Disbursement

## Tech Stack
This system has used the following technologies;
- Laravel: Provides the backend 
- Vue JS: For building UI components
- Inertia: For routing
- MySQL: Database. Persit system data.

## Setup
cd to the project root folder and run the following commands on the terminal:
- **npm install**: Installs js dependencies
- **composer install**: Installs PHP dependencies
- **php artisan setup**: Creates DB tables
- **npm run dev**: Start js auto build during development
- **php artisan serve**: Starts application local server for testing

This command migrates(creates) all db migrations a fresh and invokes AdminSeeder which creates the default system admin. It also clears cache.

### Default Admin Credentials
The following are the default admin credentials;
- **Username: admin@admin.com**
- **Password: admin**

## DB Connection
Update the following parameters in .env file in the project root folder. Note if the .env file is not found copy contents of .env.example and create a new .env file.

- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=loansmanager
- DB_USERNAME=root
- DB_PASSWORD=


